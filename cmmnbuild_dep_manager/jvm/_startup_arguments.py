"""
Functionality for configuring JVM startup arguments.

"""
# If it becomes useful, it should be possible for code in this module to be
# uplift into JPype itself.
# Therefore this module must not depend on other parts of cmmnbuild-dep-manager.
import dataclasses
import os
import typing

# Note: No imports of cmmnbuild_dep_manager in this module.


@dataclasses.dataclass
class JVMStartupArguments:
    """
    Manage startup arguments to be passed through to a JVM startup call with JPype.

    Attributes:

    .. autosummary::

        ~JVMStartupArguments.jvm_path
        ~JVMStartupArguments.stack_size
        ~JVMStartupArguments.min_heap_size
        ~JVMStartupArguments.max_heap_size
        ~JVMStartupArguments.disable_signal_handling

        ~JVMStartupArguments.class_path
        ~JVMStartupArguments.frozen

    Note: This class does *not* validate the values that are given to it by design.
    Starting the JVM is the only way to know if the values that you are passing
    through to the JVM are valid.

    """
    #: The path to the JVM.
    jvm_path: typing.Union[None, str]

    #: The stack size (``-Xss``).
    stack_size: typing.Optional[str]

    #: The minimum heap size (``-Xms``).
    min_heap_size: typing.Optional[str]

    #: The maximum heap size (``-Xmx``).
    max_heap_size: typing.Optional[str]

    #: Whether to disable signal handling (``-Xrs``).
    disable_signal_handling: typing.Optional[bool]

    #: The JARs which should be added to the class path (list of strings).
    class_path: typing.List[str]

    #: If True the JVM startup arguments can no longer be changed.
    frozen: bool

    _ATTR_TO_JVM_ARG = {
        'stack_size': '-Xss',
        'min_heap_size': '-Xms',
        'max_heap_size': '-Xmx',
    }

    def __init__(self) -> None:
        """"""
        super().__setattr__('frozen', False)
        for field in dataclasses.fields(self):
            if field.name not in {'frozen'}:
                setattr(self, field.name, None)
        self.class_path: typing.List[str] = []

    def compute_JVM_args(self) -> typing.List[typing.Any]:
        """
        Return the arguments as they should be passed to :func:`jpype.startJVM`.

        """
        args = [self.jvm_path]  # None allowed here since JPype 1.1+.
        for field in dataclasses.fields(self):
            name = field.name
            if name in {'jvm_path', 'frozen'}:
                continue
            val = getattr(self, field.name)
            if val is not None:
                if name == 'disable_signal_handling':
                    if val:
                        args.append('-Xrs')
                elif field.name == 'class_path':
                    if val:
                        args.append(f'-Djava.class.path={join_class_path(val)}')
                else:
                    arg_name = self._ATTR_TO_JVM_ARG[field.name]
                    args.append(f'{arg_name}{val}')
        return args

    def __setattr__(self, key: str, value: typing.Any) -> None:
        if self.frozen:
            raise ValueError(f"Unable to set {key} as the arguments have been frozen")
        return super().__setattr__(key, value)


def join_class_path(jars: typing.List[str]) -> str:
    """Returns a delimited string suitable for java.class.path"""
    jar_sep = ';' if os.name == 'nt' else ':'
    return jar_sep.join(jars)
