import psutil

KiB: int = 2 ** 10
MiB: int = KiB ** 2
GiB: int = KiB ** 3


def choose_heap_max(machine_memory: int) -> int:
    """
    Pick a reasonable default heap max memory, in bytes, for the JVM.

    The algorithm is an implementation detail, but currently picks 2/3 of
    the machine's physical memory, capped at 8GiB (to avoid taking more
    than necessary on large shared machines).

    Parameters
    ----------
    machine_memory: int
        The number of bytes of memory available to the machine.

    Notes
    -----

    Reference: https://stackoverflow.com/a/13310792/741316

    Find out your max heap size with::

        $ java  -XshowSettings:vm -version
        VM settings:
            Max. Heap Size (Estimated): 3.85G
            Using VM: OpenJDK 64-Bit Server VM

    """
    max_heap_size = machine_memory * 2/3
    # Clip the memory to 8GiB.
    max_heap_size = min([max_heap_size, 8 * GiB])
    return int(max_heap_size)


def machine_memory() -> int:
    """Return the total amount of memory that this machine has."""
    return psutil.virtual_memory().total
