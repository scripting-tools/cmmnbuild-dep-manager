import logging
import os
import threading

import jpype as jp

from . import _startup_arguments

LOG = logging.getLogger(__package__)


class JVMManager:
    """
    The entity with the sole responsibility for starting a JPype JVM in cmmnbuild-dep-manager.

    Attributes:

    .. autosummary::

        JVMManager.startup_arguments

    """
    def __init__(self) -> None:
        """Initialise the JVM managing instance."""
        #: Control of the JVM startup arguments (a
        #: :class:`~cmmnbuild_dep_manager.jvm._startup_arguments.JVMStartupArguments` instance).
        self.startup_arguments = _startup_arguments.JVMStartupArguments()
        self._jvm_started_by_me = False

    def required(self) -> None:
        """Start the JVM if it isn't already running."""
        if jp.isJVMStarted():
            if not self._jvm_started_by_me:
                raise RuntimeError(
                    "The JVM has not been started by this manager. "
                    "Unable to confirm that it fits the JVM requirements.",
                )

            # JVM is running, and it was done by this JVMManager instance.
            return

        if 'JAVA_JVM_LIB' in os.environ:
            LOG.warning(
                'The JAVA_JVM_LIB environment variable is no longer considered when '
                'starting the JVM with cmmnbuild_dep_manager. '
                'Please set Manager.JVM.startup_arguments.jvm_path instead.',
            )

        jvm_args = self.startup_arguments.compute_JVM_args()
        jp.startJVM(*jvm_args, convertStrings=True)

        self._jvm_started_by_me = True
        # Prevent further startup argument changes.
        if not self.startup_arguments.frozen:
            self.startup_arguments.frozen = True

        # Attach the thread as daemon if not the main thread.
        # See https://github.com/jpype-project/jpype/issues/1169.
        if threading.current_thread() is not threading.main_thread():
            jp.java.lang.Thread.detach()
            jp.java.lang.Thread.attachAsDaemon()
