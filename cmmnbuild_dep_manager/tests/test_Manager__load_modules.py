from contextlib import contextmanager
import json
import logging
import sys
import types
import typing
import unittest.mock

import pytest

from .utils import (
    add_cmmnbuild_entrypoint,
    create_fake_dist,
    mock_importlib_distributions,
)

if sys.version_info >= (3, 10):
    import importlib.metadata as importlib_metadata
else:
    import importlib_metadata as importlib_metadata


@contextmanager
def fake_entrypoints(target_entrypoints: typing.Dict[str, typing.List[str]]):
    def new_entrypoints_w_group_kwarg(*, group):
        if group not in target_entrypoints:
            raise RuntimeError(
                'Unexpected arguments to entrypoints.get_group_all',
            )
        return target_entrypoints[group]

    if sys.version_info >= (3, 10):
        patch_target = 'importlib.metadata.entry_points'
    else:
        patch_target = 'importlib_metadata.entry_points'

    new_entrypoints = unittest.mock.Mock(side_effect=new_entrypoints_w_group_kwarg)
    with unittest.mock.patch(patch_target, new_entrypoints):
        yield


@contextmanager
def tmp_modules_content(modules_content, modules_json_path):
    existing = None
    if modules_json_path.exists():
        with modules_json_path.open('rt') as fh:
            existing = fh.read()
    with modules_json_path.open('wt') as fh:
        json.dump(modules_content, fh)
    try:
        yield
    finally:
        if existing is not None:
            with modules_json_path.open('wt') as fh:
                fh.write(existing)


class FakePkgMgr:
    # TODO: Remove this.
    def __init__(self) -> None:
        #: A mapping of package names to fake module instance.
        self.pkgs: typing.Dict[str, types.ModuleType] = {}

        #: A mapping from package name to the registered entrypoint.
        self.entry_points: typing.Dict[str, importlib_metadata.EntryPoint] = {}

    @classmethod
    @contextmanager
    def fake_pkg(cls, name, version, entrypoint):
        self = cls()
        mod = self.add_module(name, version, entrypoint)
        with self:
            yield mod

    def add_module(self, name, version, entrypoint: typing.Optional[str] = None) -> types.ModuleType:
        mod = self.pkgs[name] = types.ModuleType(name)

        if entrypoint:
            self.entry_points[name] = importlib_metadata.EntryPoint(
                value=entrypoint, name=name, group='cmmnbuild_dep_manager',
            )
        return mod

    def __enter__(self):
        for pkg, mod in self.pkgs.items():
            # Note this will replace any that already exist, so some caution
            # needed.
            sys.modules[pkg] = mod

    def __exit__(self, type, value, traceback):
        for pkg in self.pkgs:
            sys.modules.pop(pkg)


def test_pkg_needs_install_simple(caplog, tmp_path, manager_cls):
    dist = create_fake_dist(tmp_path, 'fake_cmmnbuild_pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist)
    with mock_importlib_distributions([dist]):
        mgr = manager_cls()
        r = mgr._load_modules()

    assert r == {'fake_cmmnbuild_pkg': ""}
    assert caplog.record_tuples == []


def test_pkg_needs_install_bad_entrypoint(caplog, tmp_path, manager_cls):
    # Note that the version of the module and the version in the entry-point
    # differ.
    dist = create_fake_dist(tmp_path, 'fake_cmmnbuild_pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist, value="1.2.4")
    with mock_importlib_distributions([dist]):
        mgr = manager_cls()
        r = mgr._load_modules()

    assert r == {'fake_cmmnbuild_pkg': ""}
    assert caplog.record_tuples == []


def test_pkg_already_installed(caplog, tmp_path, manager_cls):
    dist = create_fake_dist(tmp_path, 'fake_cmmnbuild_pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist, value="1.2.3")
    with mock_importlib_distributions([dist]):
        with tmp_modules_content({'fake_cmmnbuild_pkg': '1.2.3'}, manager_cls.test_modules_json):
            mgr = manager_cls()
            r = mgr._load_modules()

    assert r == {'fake_cmmnbuild_pkg': "1.2.3"}
    assert caplog.record_tuples == []


@pytest.mark.parametrize("version_in_entrypoint", [True, False])
def test_pkg_needs_update(caplog, tmp_path, manager_cls, version_in_entrypoint):
    # Note that the version of the module and the version in the modules.json
    # differ.
    dist = create_fake_dist(tmp_path, 'fake_cmmnbuild_pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist, value="1.2.3" if version_in_entrypoint else 'os')
    with mock_importlib_distributions([dist]):
        with tmp_modules_content({'fake_cmmnbuild_pkg': '1.2.2'}, manager_cls.test_modules_json):
            mgr = manager_cls()
            r = mgr._load_modules()

    assert r == {'fake_cmmnbuild_pkg': ""}
    assert caplog.record_tuples == [
        (
            'cmmnbuild_dep_manager', logging.WARN,
            'fake_cmmnbuild_pkg is being updated from 1.2.2 to 1.2.3',
        ),
    ]


def test_pkg_no_update_bad_entrypoint(caplog, tmp_path, manager_cls):
    # Note that the version of the module and the version in the entry-point
    # differ.
    dist = create_fake_dist(tmp_path, 'fake_cmmnbuild_pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist)
    with mock_importlib_distributions([dist]):
        with tmp_modules_content({'fake_cmmnbuild_pkg': '1.2.2'}, manager_cls.test_modules_json):
            mgr = manager_cls()
            r = mgr._load_modules()

    assert r == {'fake_cmmnbuild_pkg': ""}
    assert caplog.record_tuples == [
        (
            'cmmnbuild_dep_manager', logging.WARN,
            'fake_cmmnbuild_pkg is being updated from 1.2.2 to 1.2.3',
        ),
    ]


def test_pkg__entrypoint_name_is_the_import_name(caplog, tmp_path, manager_cls):
    # Note that the name is not the same
    dist = create_fake_dist(tmp_path, 'fake-cmmnbuild-pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist, name='fake_cmmnbuild_pkg')

    with mock_importlib_distributions([dist]):
        mgr = manager_cls()
        r = mgr._load_modules()

    assert r == {'fake_cmmnbuild_pkg': ""}
