from cmmnbuild_dep_manager.jvm._startup_arguments import JVMStartupArguments


def test_jvm_path():
    args = JVMStartupArguments()
    args.jvm_path = 'some/path'
    assert 'some/path' == args.compute_JVM_args()[0]
    args.jvm_path = None
    assert None is args.compute_JVM_args()[0]


def test_max_heap_size():
    args = JVMStartupArguments()
    args.max_heap_size = '9g'
    expected = '-Xmx9g'
    assert expected in args.compute_JVM_args()
    args.max_heap_size = None
    assert expected not in args.compute_JVM_args()


def test_min_heap_size():
    args = JVMStartupArguments()
    args.min_heap_size = '9g'
    expected = '-Xms9g'
    assert expected in args.compute_JVM_args()
    args.min_heap_size = None
    assert expected not in args.compute_JVM_args()


def test_stack_size():
    args = JVMStartupArguments()
    args.stack_size = '5m'
    expected = '-Xss5m'
    assert expected in args.compute_JVM_args()
    args.stack_size = None
    assert expected not in args.compute_JVM_args()


def test_disable_signal_handling():
    args = JVMStartupArguments()
    expected = '-Xrs'
    assert expected not in args.compute_JVM_args()
    args.disable_signal_handling = True
    assert expected in args.compute_JVM_args()
    args.disable_signal_handling = False
    assert expected not in args.compute_JVM_args()
