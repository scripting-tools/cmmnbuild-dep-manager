import pytest

from cmmnbuild_dep_manager.jvm import _memory


@pytest.mark.parametrize(
    "test_input, expected", [
        (10000, 6666),
        # The number should be capped at 8GiB
        (15 * _memory.GiB, 8 * _memory.GiB),
    ],
)
def test_memory(test_input, expected):
    assert _memory.choose_heap_max(test_input) == expected
