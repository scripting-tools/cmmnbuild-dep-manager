import argparse
from unittest import mock

import pytest

import cmmnbuild_dep_manager.__main__ as main


@pytest.fixture
def parser():
    parser = argparse.ArgumentParser()
    main.configure_parser(parser)
    return parser


def test_no_args(parser, capsys):
    args = parser.parse_args([])
    with pytest.raises(SystemExit):
        args.handler(args)
    captured = capsys.readouterr()
    assert captured.out == parser.format_help()


def test_duplicate_kwargs(parser):
    args = parser.parse_args(['is_registered', 'name1', '--name=name2'])
    with pytest.raises(ValueError, match='Duplicate parameter "name" provided'):
        args.handler(args)


def test_list(parser, capsys):
    args = parser.parse_args(['list'])

    new_list = mock.Mock(return_value=['item 1', 'another_item'])
    with mock.patch('cmmnbuild_dep_manager.Manager.list', new_list):
        args.handler(args)
    captured = capsys.readouterr()
    assert captured.out == "item 1\nanother_item\n"


def test_registered_modules(parser, capsys):
    args = parser.parse_args(['registered_modules'])

    new_list = mock.Mock(return_value=['item 1', 'another_item'])
    with mock.patch('cmmnbuild_dep_manager.Manager.registered_modules', new_list):
        args.handler(args)
    captured = capsys.readouterr()
    assert captured.out == "item 1\nanother_item\n"


def test_register(parser, capsys):
    args = parser.parse_args(['register', 'arg 1', 'arg_two'])

    register_fn = mock.Mock(return_value="")
    with mock.patch('cmmnbuild_dep_manager.Manager.register', register_fn):
        args.handler(args)
    register_fn.assert_called_once_with('arg 1', 'arg_two')
    captured = capsys.readouterr()
    assert captured.out == "\n"


def test_class_path(parser, capsys):
    args = parser.parse_args(['class_path'])

    class_path_fn = mock.Mock(return_value='/path/to/classes')
    with mock.patch('cmmnbuild_dep_manager.Manager.class_path', class_path_fn):
        args.handler(args)
    captured = capsys.readouterr()
    assert captured.out == f"{class_path_fn.return_value}\n"


def test_is_installed(parser, capsys):
    args = parser.parse_args(['is_installed'])

    # Neither a string nor an iterable return value has a special code path.
    is_installed_fn = mock.Mock(return_value=False)
    with mock.patch('cmmnbuild_dep_manager.Manager.is_installed', is_installed_fn):
        args.handler(args)
    captured = capsys.readouterr()
    assert captured.out == "False\n"
