from contextlib import contextmanager
import sys
import typing
import unittest.mock

if sys.version_info >= (3, 10):
    import importlib.metadata as importlib_metadata
else:
    import importlib_metadata as importlib_metadata

import pathlib
import textwrap


@contextmanager
def mock_packages_distributions(return_value):
    if sys.version_info < (3, 10):
        dist_name = 'importlib_metadata.packages_distributions'
    else:
        dist_name = 'importlib.metadata.packages_distributions'
    with unittest.mock.patch(dist_name, return_value=return_value):
        yield


@contextmanager
def mock_importlib_distributions(dists: typing.List[importlib_metadata.Distribution]):
    if sys.version_info >= (3, 10):
        dists_target = 'importlib.metadata.distributions'
        dist_target = 'importlib.metadata.distribution'
    else:
        dists_target = 'importlib_metadata.distributions'
        dist_target = 'importlib_metadata.distribution'

    def new_distribution_fn(name: str):
        for dist in dists:
            if dist.name == name:
                return dist
        else:
            raise ImportError(f'Dist {name} not found')

    with unittest.mock.patch(dists_target, return_value=dists) as dists_m:
        with unittest.mock.patch(dist_target, new_distribution_fn) as dist_m:
            yield dists_m, dist_m


def create_fake_dist(tmp_path: pathlib.Path, name: str, version: str) -> importlib_metadata.Distribution:
    dist_path = tmp_path / 'dists' / name / version / f'{name}-{version}.dist-info'
    dist_path.mkdir(exist_ok=False, parents=True)
    meta_path = dist_path / 'METADATA'
    content = (
        textwrap.dedent(f'''\
        Metadata-Version: 2.1
        Name: {name}
        Version: {version}
    ''') + '\n'
    )
    meta_path.write_text(content, encoding='utf-8')
    dist = importlib_metadata.PathDistribution(dist_path)
    return dist


def add_cmmnbuild_entrypoint(
        dist: importlib_metadata.Distribution,
        name=None,
        group='cmmnbuild_dep_manager',
        value='site',
):
    if name is None:
        name = dist.name
    dist_dir = get_dist_dir(dist)
    ep_file = dist_dir / 'entry_points.txt'
    assert not ep_file.exists()
    ep_file.write_text(
        textwrap.dedent(f'''\
        [{group}]
        {name} = {value}
    '''),
    )


def get_dist_dir(dist: importlib_metadata.Distribution) -> pathlib.Path:
    # Return the dist-info directory.
    return dist._path  # type: ignore[attr-defined]  # noqa
