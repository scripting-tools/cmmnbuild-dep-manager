from __future__ import annotations

import pathlib
import typing
from unittest import mock

import pytest

import cmmnbuild_dep_manager.resolver as resolver_mod
import cmmnbuild_dep_manager.resolver.cbng_web as cbng_web


def mock_resolver_cls(
        resolved_artifacts: typing.Tuple[resolver_mod.Artifact, ...],
) -> typing.Type[cbng_web.CbngWebResolver]:

    class TestCbngWebResolver(cbng_web.CbngWebResolver):
        def __init__(self, *args, **kwargs):
            self.resolved_artifacts = resolved_artifacts
            self.init_args = args
            self.init_kwargs = kwargs

    return TestCbngWebResolver


@pytest.fixture
def mocked_artifact_download():
    class TestArtifact(resolver_mod.Artifact):
        def download_jar(self, destination: pathlib.Path):
            jar_filename = self.filename_from_uri
            create_mock_jar_file(destination, jar_filename)

    with mock.patch('cmmnbuild_dep_manager.resolver.Artifact', TestArtifact):
        yield


@pytest.fixture
def resolver_w_2_artifacts(mocked_artifact_download):
    unused_kwargs = {'artifact_id': '', 'group_id': '', 'version': ''}
    resolver = mock_resolver_cls((
        resolver_mod.Artifact(uri='www.some.where/group/atr/todownload.jar', **unused_kwargs),
        resolver_mod.Artifact(uri='www.some.where/group/atr/existing.jar', **unused_kwargs),
    ))
    yield resolver


def create_mock_jar_file(lib_dir: pathlib.Path, filename: str, content: str = "") -> None:
    jar_file_path = lib_dir / filename
    jar_file_path.write_text(content)


def test_resolve_and_download(tmp_lib_dir):
    resolver = cbng_web.CbngWebResolver([{"product": "japc", "groupId": "cern.japc"}])
    resolver.save_jars(tmp_lib_dir)

    for dep in resolver.resolved_artifacts:
        jar_file = tmp_lib_dir / dep.filename_from_uri
        assert jar_file.exists()
        # Check if file is not empty
        assert jar_file.stat().st_size > 0


def test_resolver_download_all_jars_when_libs_empty(tmp_lib_dir, resolver_w_2_artifacts):
    resolver_w_2_artifacts().save_jars(tmp_lib_dir)
    assert (tmp_lib_dir / "todownload.jar").exists()


def test_resolver_download_only_missing_jars(tmp_lib_dir, resolver_w_2_artifacts):
    create_mock_jar_file(tmp_lib_dir, "existing.jar", content="jar_content")
    resolver_w_2_artifacts().save_jars(tmp_lib_dir)
    # Check previously existing file content wasn't edited
    assert (tmp_lib_dir/"existing.jar").read_text() == 'jar_content'


def test_resolver_removes_unnecessary_jars(tmp_lib_dir, resolver_w_2_artifacts):
    create_mock_jar_file(tmp_lib_dir, "unwanted.jar")
    resolver_w_2_artifacts().save_jars(tmp_lib_dir)
    assert not (tmp_lib_dir / "unwanted.jar").exists()


def test_resolved_artifacts__filename_from_uri():
    artifact = resolver_mod.Artifact('pkg', 'group-id', '321.123.55+1', 'https://uri/thing.jar')
    assert artifact.filename_from_uri == 'thing.jar'
