import threading

import jpype as jp
import pytest

from cmmnbuild_dep_manager.jvm._manager import JVMManager


@pytest.mark.run_in_subprocess
def test_required__threadded_non_main():
    def main():
        mgr = JVMManager()
        mgr.required()

        assert jp.java.lang.Thread.isAttached()
        assert jp.java.lang.Thread.currentThread().isDaemon()

    t = threading.Thread(target=main, name='jvm-starter')
    t.start()
    t.join()

    assert jp.isJVMStarted()
    assert not jp.java.lang.Thread.isAttached()


@pytest.mark.run_in_subprocess
def test_required__threadded_main():
    mgr = JVMManager()
    mgr.required()

    assert jp.isJVMStarted()
    assert jp.java.lang.Thread.isAttached()
    # On main, we let JPype decide if it is daemon or not. It is not daemon
    # as of JPype 1.5.
    assert not jp.java.lang.Thread.currentThread().isDaemon()
