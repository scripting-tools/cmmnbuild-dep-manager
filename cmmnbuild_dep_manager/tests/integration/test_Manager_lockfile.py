import json
from pathlib import Path
from unittest import mock

import pytest

from ..test_cmmnbuild_dep_manager import mod3_w_groupid_deps  # noqa
from ..test_cmmnbuild_dep_manager import mod_w_simple_deps
from ..utils import (
    add_cmmnbuild_entrypoint,
    get_dist_dir,
    mock_importlib_distributions,
    mock_packages_distributions,
)
from .test_cbng_web_service_resolver import mocked_artifact_download  # noqa
from .test_cbng_web_service_resolver import resolver_w_2_artifacts


@pytest.fixture
def manager_w_jar_deps(manager_cls, mod_w_simple_deps, mod3_w_groupid_deps):
    mgr = manager_cls()

    with mod_w_simple_deps as (mod, d1), mod3_w_groupid_deps as (mod2, d2):
        add_cmmnbuild_entrypoint(d1)
        add_cmmnbuild_entrypoint(d2)
        mgr.mod1, mgr.mod2 = mod, mod2
        mgr.d1, mgr.d2 = d1, d2
        with mock_importlib_distributions([d1, d2]), mock_packages_distributions({mod.__name__: [d1.name], mod2.__name__: [d2.name]}):
            mod.__cmmnbuild_deps__ = mod.__test_resolver_deps__
            mod2.__cmmnbuild_deps__ = mod2.__test_resolver_deps__
            yield mgr


def test_empty_environment(manager_w_jar_deps, resolver_w_2_artifacts, tmpdir):
    mgr = manager_w_jar_deps

    mgr._resolver = resolver_w_2_artifacts

    with mock.patch('pathlib.Path.cwd', return_value=Path(tmpdir)):
        mgr.lockfile_generate()

    lockfile = tmpdir / 'cmmnbuild-dep-manager.lock'
    assert lockfile.exists()

    with lockfile.open('rt') as fh:
        result = json.load(fh)

    expected = {
        "python_modules": {
            'mod3_w_groupid_deps': '1.2.3', 'mod_w_simple_deps': '1.2.3',
        },
        "java_requirements": [
            'java_dep1', {'artifactId': 'java_dep2', 'groupId': 'something.else'},
            'java_dep1', 'java_dep2',
        ],
        "resolved_artifacts": [
            {
                "artifact_id": "",
                "group_id": "",
                "version": "",
                "uri": "www.some.where/group/atr/todownload.jar",
            },
            {
                "artifact_id": "",
                "group_id": "",
                "version": "",
                "uri": "www.some.where/group/atr/existing.jar",
            },
        ],
    }
    assert expected == result
    with mock.patch('pathlib.Path.cwd', return_value=Path(tmpdir)):
        mgr.lockfile_install()

    assert mgr.is_resolved()

    assert mgr.jars() == [
        f'{mgr.jar_path()}/{fname}' for fname in ['existing.jar', 'todownload.jar']
    ]


def test_jar_requirements_changed(manager_w_jar_deps, resolver_w_2_artifacts, tmp_path, mod_w_simple_deps):
    mgr = manager_w_jar_deps
    mgr._resolver = resolver_w_2_artifacts

    lockfile = tmp_path / 'cmmnbuild-dep-manager.lock'
    mgr.lockfile_generate(lockfile_path=lockfile)

    lock_contents = json.loads(lockfile.read_text())
    assert lock_contents['python_modules'] == {'mod3_w_groupid_deps': '1.2.3', 'mod_w_simple_deps': '1.2.3'}

    # If we remove one of the modules which made up the lockfile (by removing
    # its entry-point), we shouldn't be allowed to install the lockfile.
    (get_dist_dir(mgr.d1) / 'entry_points.txt').unlink()

    with pytest.raises(ValueError, match="The Java requirements from the lockfile do not match"):
        mgr.lockfile_install(lockfile_path=lockfile)

    assert not mgr.is_resolved()
