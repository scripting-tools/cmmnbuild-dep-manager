import os
from pathlib import Path
import shutil
import subprocess
from unittest import mock

import packaging.version
import pytest

from cmmnbuild_dep_manager.resolver.gradle import GradleResolver


def build_gradle_of(resolver: GradleResolver) -> str:
    # Get the contents of build.gradle for a given resolver.
    gradle_dir = Path(resolver.gradle_dir)
    return (gradle_dir / 'build.gradle').read_text()


@pytest.mark.parametrize(
    ["specification", "expected_spec"],
    [
        ('junit:junit:4.12', 'junit:junit:4.12'),
        ({'artifactId': 'junit', 'groupId': 'junit', 'version': '4.12'}, 'junit:junit:4.12'),
        ({'product': 'junit', 'groupId': 'junit'}, 'junit:junit:+'),
        ({'groupId': 'org.springframework', 'product': 'spring-bom', 'version': '42', 'type':'platform'}, "platform('org.springframework:spring-bom:42')"),
        ("enforcedPlatform('org.springframework:spring-bom:42')", "enforcedPlatform('org.springframework:spring-bom:42')"),
        ("enforcedPlatform(org.springframework:spring-bom:42)", "enforcedPlatform('org.springframework:spring-bom:42')"),
    ],
)
def test_constructor(specification, expected_spec):
    with mock.patch('subprocess.call', return_value=0):
        resolver = GradleResolver([specification])

    assert expected_spec in build_gradle_of(resolver)


def test_constructor_invalid_characters(caplog):
    with mock.patch('subprocess.call', return_value=0):
        GradleResolver(['not valid@thing'])
    assert (
        'IGNORING __gradle_deps__ dependency "not valid@thing": '
        'it is not in gradle format'
    ) in caplog.text


def test_constructor_not_gradle_format(caplog):
    with mock.patch('subprocess.call', return_value=0):
        GradleResolver(['not valid'])
    assert (
        'IGNORING __gradle_deps__ dependency "not valid": '
        'it is not in gradle format'
    ) in caplog.text


def test_constructor_dep_repo():
    with mock.patch('subprocess.call', return_value=0):
        resolver = GradleResolver([
            {
                'groupId': 'foo',
                'artifactId': 'bar',
                'repository': '''maven { url "http://artifacts/content/groups/public/" }''',
            },
        ])

    gradle_dir = Path(resolver.gradle_dir)
    content = (gradle_dir / 'build.gradle').read_text()
    assert 'artifacts/content' in content


def get_java_path() -> Path:
    # jpype.getDefaultJVMPath() returns the libjvm.so path. We want the executable.
    java_home = os.environ.get('JAVA_HOME')
    if java_home is not None:
        java = Path(java_home) / 'bin' / 'java'
    else:
        java_bin = shutil.which('java')
        if java_bin is None:
            raise RuntimeError("Unable to find Java on the PATH")
        java = Path(java_bin)
    if not java.exists():
        raise RuntimeError("Unable to find Java for testing")
    return java


def get_java_version(java_bin) -> packaging.version.Version:
    """
    Return a :class:`packaging.version.Version` instance for the Java version
    of the given Java executable.

    Calls a subprocess with ``{java_bin} -version`` and parses the output.
    The versions for java 8 will look like ``1.8``, and for Java 11 ``11.0``.

    """
    output = subprocess.check_output([java_bin, '-version'], stderr=subprocess.STDOUT).decode('utf8')
    [version_line] = [line for line in output.split('\n') if 'version' in line]
    implementation, version = version_line.replace('"', '').split(' version ')
    major, minor, _ = version.split('.', 2)
    return packaging.version.Version(f'{major}.{minor}')


def test_full_gradle(tmp_path):
    version = get_java_version(get_java_path())
    if version > packaging.version.Version("1.8"):
        pytest.skip("Java >8 not supported for the Gradle resolver")

    resolver = GradleResolver(
        [
            'junit:junit:4.12',
            {'artifactId': 'maven-core', 'groupId': 'org.apache.maven'},
        ],
    )
    assert len(list(tmp_path.glob('**/*.jar'))) == 0
    resolver.save_jars(tmp_path)
    assert len(list(tmp_path.glob('**/*.jar'))) > 2
