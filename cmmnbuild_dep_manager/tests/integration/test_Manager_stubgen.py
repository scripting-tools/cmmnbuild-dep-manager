import logging

import pytest

from ..test_Manager__load_modules import FakePkgMgr

try:
    import stubgenj
except ImportError:
    pytest.skip(
        "stubgenj tests require the stubgen optional extra",
        allow_module_level=True,
    )


def test_stubgen(caplog, manager_cls, tmpdir):
    caplog.set_level(logging.INFO)
    with FakePkgMgr.fake_pkg("fake_cmmnbuild_pkg", "1.2.3", '1.2.3') as mod:
        mod.__stubgen_packages__ = ('java.util', 'java.lang')
        mgr = manager_cls()

        assert mgr._stubgen_packages() == {'java.util', 'java.lang'}
        mgr.stubgen(tmpdir)

    # Check that we got the log message telling us where the stubs went.
    assert (
        'cmmnbuild_dep_manager', logging.INFO,
        f'Generated stubs available at {tmpdir}',
    ) in caplog.record_tuples

    # At no point do we mention java.lang.reflect, but such a stub should exist
    # as a result of declaring java.lang a stubgen package.
    assert (tmpdir / 'java-stubs' / 'lang' / 'reflect' / '__init__.pyi').exists()


def test_no_stubgen_packages(caplog, manager_cls, tmpdir):
    caplog.set_level(logging.INFO)
    with FakePkgMgr.fake_pkg("fake_cmmnbuild_pkg", "1.2.3", '1.2.3') as mod:
        mgr = manager_cls()
        mgr.stubgen(tmpdir)

    assert (
        'cmmnbuild_dep_manager', logging.INFO,
        'No packages registered for stub generation',
    ) in caplog.record_tuples
    assert not (tmpdir / 'java-stubs').exists()
