from __future__ import annotations

import sys
import typing
from unittest.mock import patch

import pytest

import cmmnbuild_dep_manager


@pytest.fixture(scope='function')
def manager_cls(tmp_lib_dir) -> typing.Generator[typing.Type[cmmnbuild_dep_manager.Manager]]:
    class TestManager(cmmnbuild_dep_manager.Manager):
        test_modules_json = tmp_lib_dir.parent / 'modules.json'

        def jar_path(self):
            return tmp_lib_dir

        def _user_dir(self):
            return tmp_lib_dir.parent

        def _dist_dir(self):
            return tmp_lib_dir.parent

    if sys.version_info >= (3, 10):
        patch_target = 'importlib.metadata.distributions'
    else:
        patch_target = 'importlib_metadata.distributions'
    with patch(patch_target, return_value=[]):
        with patch('cmmnbuild_dep_manager._manager.PKG_MODULES_JSON', tmp_lib_dir.parent / 'modules.json'):
            yield TestManager


@pytest.fixture(scope='session', autouse=True)
def no_cbng_web_ping_allowed():
    with patch(
        'cmmnbuild_dep_manager.resolver.cbng_web.CbngWebResolver.is_available',
            side_effect=RuntimeError('The is_available method was called on the CbngWebResolver'),
    ):
        yield


@pytest.fixture(scope='function')
def tmp_lib_dir(tmp_path):
    tmp_dir = tmp_path / "lib"
    tmp_dir.mkdir()
    return tmp_dir
