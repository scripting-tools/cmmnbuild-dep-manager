from __future__ import annotations

from contextlib import contextmanager
import logging
import os
import pathlib
from pathlib import Path
import site
import sys
import types
import typing
import unittest.mock

import pytest

from cmmnbuild_dep_manager.resolver import Resolver

from .test_Manager__load_modules import tmp_modules_content
from .utils import (
    add_cmmnbuild_entrypoint,
    create_fake_dist,
    mock_importlib_distributions,
    mock_packages_distributions,
)

if sys.version_info >= (3, 10):
    import importlib.metadata as importlib_metadata
else:
    import importlib_metadata as importlib_metadata


class SimpleResolver(Resolver):
    dependency_variable = '__test_resolver_deps__'
    description = 'The test resolver which does nothing'

    @classmethod
    def is_available(cls):
        return True

    def __init__(self, dependencies):
        pass

    def save_jars(self, dir):
        pass

    @classmethod
    def get_help(cls, classnames, class_info):
        raise ValueError('No help available from this testable resolver')


@pytest.fixture
def simple_resolver():
    with unittest.mock.patch(
            'cmmnbuild_dep_manager.resolver.resolvers',
            return_value=[SimpleResolver],
    ):
        yield


@contextmanager
def tmp_mod(tmp_path: pathlib.Path, name: str, dependencies=None, version="1.2.3") -> typing.Generator[typing.Tuple[types.ModuleType, importlib_metadata.Distribution]]:
    """
    Generate a temporary module that is suitable for testing dependency
    resolution with.

    """
    mod = types.ModuleType('a_cmmnbuild_test_module')
    if dependencies is not None:
        mod.__test_resolver_deps__ = dependencies  # type: ignore[attr-defined]

    mod.__name__ = name

    dist = create_fake_dist(tmp_path, name=name, version=version)

    sys.modules[name] = mod
    try:
        yield mod, dist
    finally:
        sys.modules.pop(name)


def test_no_such_module(caplog, tmp_path, manager_cls, simple_resolver):
    with tmp_mod(tmp_path, 'this_mod_doesnt_exist', version='1.2.3') as (m1, d1):
        with mock_importlib_distributions([d1]):
            add_cmmnbuild_entrypoint(d1)
            manager_cls().resolve()

    assert caplog.record_tuples == [
        (
            "cmmnbuild_dep_manager", logging.ERROR,
            'this_mod_doesnt_exist not found',
        ),
        (
            "cmmnbuild_dep_manager", logging.ERROR,
            'no dependencies were found',
        ),
    ]


def test_module_missing_special_resolver_attr(caplog, tmp_path, manager_cls, simple_resolver):
    with tmp_mod(tmp_path, 'a_cmmnbuild_test_module', version='1.2.3') as (_, d1):
        with mock_importlib_distributions([d1]), mock_packages_distributions({'a_cmmnbuild_test_module': ['a_cmmnbuild_test_module']}):
            add_cmmnbuild_entrypoint(d1)
            manager_cls().resolve()
    warning = (
        "cmmnbuild_dep_manager", logging.ERROR,
        "a_cmmnbuild_test_module doesn't declare a __test_resolver_deps__ "
        "attribute, which is needed by the SimpleResolver resolver to get "
        "its required Java dependencies",
    )
    assert warning in caplog.record_tuples


def test_resolve_missing_module_unregister(caplog, tmp_path, manager_cls, simple_resolver):
    with tmp_modules_content({'missing_pkg': ''}, manager_cls.test_modules_json):
        mgr = manager_cls()
        assert mgr._load_modules() == {'missing_pkg': ''}
        # Resolving a missing package should result in it being unregistered.
        mgr.resolve()
        assert mgr._load_modules() == {}


@pytest.fixture
def mocked_mgr(manager_cls):
    """
    A fixture that provides a clean manager and resolver for mocked testing of
    module installation.

    """
    resolver = unittest.mock.Mock(
        __name__='mock_resolver',
        dependency_variable='__test_resolver_deps__',
        description="the mock resolver",
        spec=SimpleResolver,
    )

    mock_resolvers = unittest.mock.patch(
        'cmmnbuild_dep_manager.resolver.resolvers',
        return_value=[resolver],
    )

    with mock_resolvers:
        yield manager_cls(), resolver


@pytest.fixture
def mod_w_simple_deps(tmp_path):
    return tmp_mod(
        tmp_path,
        'mod_w_simple_deps', version='1.2.3',
        dependencies=['java_dep1', 'java_dep2'],
    )


@pytest.fixture
def mod2_w_simple_deps(tmp_path):
    return tmp_mod(
        tmp_path,
        'mod2_w_simple_deps', version='1.2.3',
        dependencies=['java_dep2', 'java_dep3'],
    )


@pytest.fixture
def mod3_w_groupid_deps(tmp_path):
    return tmp_mod(
        tmp_path,
        'mod3_w_groupid_deps', version='1.2.3',
        dependencies=[
            'java_dep1', {'artifactId': 'java_dep2', 'groupId': 'something.else'},
        ],
    )


@pytest.fixture
def mod_wo_deps(tmp_path):
    return tmp_mod(tmp_path, 'mod_wo_deps', version='1.2.3')


def test_module_dependencies(mocked_mgr, mod_w_simple_deps):
    mgr, m_resolver = mocked_mgr

    with mod_w_simple_deps as (m1, d1):
        with mock_importlib_distributions([d1]), mock_packages_distributions({m1.__name__: [d1.name]}):
            add_cmmnbuild_entrypoint(d1)
            mgr.resolve()

    m_resolver.assert_called_once_with(
        ('java_dep1', 'java_dep2'),
    )


def test_multi_module_dependencies(mocked_mgr, mod_w_simple_deps, mod2_w_simple_deps):
    mgr, m_resolver = mocked_mgr

    with mod_w_simple_deps as (m1, d1), mod2_w_simple_deps as (m2, d2):
        with mock_importlib_distributions([d1, d2]), mock_packages_distributions({m1.__name__: [d1.name], m2.__name__: [d2.name]}):
            add_cmmnbuild_entrypoint(d2)
            add_cmmnbuild_entrypoint(d1)
            mgr.resolve()

    m_resolver.assert_called_once_with(
        ('java_dep2', 'java_dep3', 'java_dep1', 'java_dep2'),
    )


def test_dependencies__with_groupid(mocked_mgr, mod_w_simple_deps, mod3_w_groupid_deps):
    mgr, m_resolver = mocked_mgr

    with mod_w_simple_deps as (m1, d1), mod3_w_groupid_deps as (m2, d2):
        with mock_importlib_distributions([d1, d2]), mock_packages_distributions({m1.__name__: [d1.name], m2.__name__: [d2.name]}):
            add_cmmnbuild_entrypoint(d2)
            add_cmmnbuild_entrypoint(d1)
            mgr.resolve()

    m_resolver.assert_called_once_with(
        # Note that we repeat java_dep1 here. This may change in the future.
        ('java_dep1', {'artifactId': 'java_dep2', 'groupId': 'something.else'}, 'java_dep1', 'java_dep2'),
    )


def test_java_requirements_for(mocked_mgr, mod3_w_groupid_deps):
    mgr, m_resolver = mocked_mgr

    with mod3_w_groupid_deps:
        assert mgr.java_requirements_for('mod3_w_groupid_deps') == (
            'java_dep1', {'artifactId': 'java_dep2', 'groupId': 'something.else'},
        )


def test_java_requirements_for__import_error(mocked_mgr):
    mgr, m_resolver = mocked_mgr

    with pytest.raises(ImportError):
        mgr.java_requirements_for('not_importable')


def test_java_requirements_for__attribute_error(mocked_mgr, mod_wo_deps):
    mgr, m_resolver = mocked_mgr

    with mod_wo_deps as (m1, d1):
        with pytest.raises(AttributeError):
            mgr.java_requirements_for('mod_wo_deps')


def test_java_requirements__with_groupid(mocked_mgr, mod_w_simple_deps, mod3_w_groupid_deps):
    mgr, m_resolver = mocked_mgr

    with mod_w_simple_deps as (m1, d1), mod3_w_groupid_deps as (m2, d2):
        add_cmmnbuild_entrypoint(d1)
        add_cmmnbuild_entrypoint(d2)
        with mock_importlib_distributions([d1, d2]), mock_packages_distributions({m1.__name__: [d1.name], m2.__name__: [d2.name]}):
            assert mgr.java_requirements() == (
                'java_dep1', {'artifactId': 'java_dep2', 'groupId': 'something.else'}, 'java_dep1', 'java_dep2',
            )


def test_java_requirements__import_error(caplog, mocked_mgr, mod_w_simple_deps, mod3_w_groupid_deps):
    mgr, m_resolver = mocked_mgr
    with mod3_w_groupid_deps as (m1, d1), mod_w_simple_deps as (m2, d2):
        add_cmmnbuild_entrypoint(d1)
        add_cmmnbuild_entrypoint(d2, name='not_importable')
        with mock_importlib_distributions([d1, d2]), mock_packages_distributions({m1.__name__: [d1.name], m2.__name__: [d2.name]}):
            assert mgr.java_requirements() == ('java_dep1', {'artifactId': 'java_dep2', 'groupId': 'something.else'})
    assert caplog.record_tuples == [
        (
            "cmmnbuild_dep_manager", logging.WARNING,
            'not_importable registered but not importable '
            '(message: No module named \'not_importable\')',
        ),
    ]


def test_java_requirements__attribute_error(caplog, mocked_mgr, mod_wo_deps, mod_w_simple_deps):
    mgr, m_resolver = mocked_mgr

    with mod_wo_deps as (m1, d1), mod_w_simple_deps as (m2, d2):
        add_cmmnbuild_entrypoint(d1)
        add_cmmnbuild_entrypoint(d2)
        with mock_importlib_distributions([d1, d2]), mock_packages_distributions({m1.__name__: [d1.name], m2.__name__: [d2.name]}):
            assert mgr.java_requirements() == ('java_dep1', 'java_dep2')
    assert caplog.record_tuples == [
        (
            "cmmnbuild_dep_manager", logging.WARNING,
            "mod_wo_deps doesn't declare a __test_resolver_deps__ attribute, which is "
            "needed by the mock_resolver resolver to get its required Java dependencies",
        ),
    ]


def test__module_version__dist_name_different_to_import_name(tmp_path, mocked_mgr):
    mgr, m_resolver = mocked_mgr

    with tmp_mod(tmp_path, name='my_module') as (mod, _):
        dist = create_fake_dist(tmp_path, name='my-project-name', version='1.2.3')

        with mock_packages_distributions({'my_module': ['my-project-name']}):
            with pytest.raises(importlib_metadata.PackageNotFoundError, match='No package metadata was found'):
                mgr._module_version('my_module')

            with mock_importlib_distributions([dist]):
                assert mgr._module_version('my_module') == '1.2.3'


def test_jarversion(mocked_mgr):
    mgr, m_resolver = mocked_mgr

    assert mgr._get_jarversion('/a/long/path/library-1.2.3.jar') == ('library', '1.2.3')
    assert mgr._get_jarversion('/a/long/path/lib-rary-1.2.3.jar') == ('lib-rary', '1.2.3')
    assert mgr._get_jarversion('lib-rary-1.2.3-SOMETHING.jar') == ('lib-rary', '1.2.3-SOMETHING')
    assert mgr._get_jarversion('library-1.2.3-SOME-THING.jar') == ('library', '1.2.3-SOME-THING')
    assert mgr._get_jarversion('1.2.3-4.5.6-SOME-THING.jar') == ('1.2.3', '4.5.6-SOME-THING')

    # no version number
    with pytest.raises(ValueError):
        mgr._get_jarversion('library.jar')


def test_class_path_join(mocked_mgr):
    mgr = mocked_mgr[0]
    extras = ['module1.jar', 'path/to/mod2.jar']
    with unittest.mock.patch('os.name', 'nt'):
        assert mgr.class_path(extras) == "module1.jar;path/to/mod2.jar"

    with unittest.mock.patch('os.name', 'something-else'):
        assert mgr.class_path(extras) == "module1.jar:path/to/mod2.jar"


def test_imports_removed_on_success(mocked_mgr):
    pytest.importorskip('jpype', minversion='1.0')
    mgr = mocked_mgr[0]
    with mgr.imports():
        import java.util
        assert java.util in sys.modules.values()
    assert java.util not in sys.modules.values()


def test_imports_removed_on_failure(mocked_mgr):
    pytest.importorskip('jpype', minversion='1.0')
    mgr = mocked_mgr[0]
    with pytest.raises(ValueError):
        with mgr.imports():
            import java.util
            assert java.util in sys.modules.values()
            raise ValueError()
    assert java.util not in sys.modules.values()


def test_imports_are_reentrant(mocked_mgr):
    pytest.importorskip('jpype', minversion='1.0')
    mgr = mocked_mgr[0]
    with mgr.imports():
        import java.util
        with mgr.imports():
            import java.lang
            assert java.util in sys.modules.values()
            assert java.lang in sys.modules.values()
        assert java.util in sys.modules.values()
        assert java.lang not in sys.modules.values()
    assert java.util not in sys.modules.values()
    assert java.lang not in sys.modules.values()


def test_init_manager_no_ping_cbng_web(manager_cls):
    # We shouldn't need cbng-web to be up in order to initialise a manager for
    # an already resolved environment.
    assert manager_cls()


def test_init_manager_no_ping_cbng_web_with_package_name_already_resolved(
    tmp_path,
    manager_cls,
):
    # We shouldn't need cbng-web to be up in order to initialise a manager for
    # an already resolved environment.
    dist = create_fake_dist(tmp_path, 'fake_cmmnbuild_pkg', version="1.2.3")
    add_cmmnbuild_entrypoint(dist)
    with mock_importlib_distributions([dist]), mock_packages_distributions({'fake_cmmnbuild_pkg': ['fake_cmmnbuild_pkg']}):
        with mock_packages_distributions({'fake_cmmnbuild_pkg': ['fake_cmmnbuild_pkg']}):
            with tmp_modules_content({'fake_cmmnbuild_pkg': '1.2.3'}, manager_cls.test_modules_json):
                assert manager_cls('fake_cmmnbuild_pkg')


def test_resolve_manager_pings_cbng_web(manager_cls):
    # Validate that resolving does indeed hit the cbng-web endpoint,
    # and that the no_cbng_web_ping_allowed fixture is doing the right thing.
    with pytest.raises(RuntimeError):
        assert manager_cls().resolve()


def test_Manager__site_packages_default(manager_cls, tmpdir):
    not_writable = Path(tmpdir) / 'not-writable'
    not_writable.mkdir(mode=0o444)
    writable = Path(tmpdir) / 'writable'
    writable.mkdir(mode=0o744)
    with unittest.mock.patch(
        'site.getsitepackages',
        return_value=[
            site.getusersitepackages(),
            str(not_writable),
            str(writable),
        ],
    ):
        result = manager_cls()._site_packages()
        assert result == str(not_writable)


def test_Manager__site_packages__writable(manager_cls, tmpdir):
    not_writable = Path(tmpdir) / 'not-writable'
    not_writable.mkdir(mode=0o444)
    if os.access(not_writable, os.W_OK | os.X_OK):
        pytest.skip("Unusual filesystem behaviour (seen in kubernetes gitlab-ci runners)")
    writable = Path(tmpdir) / 'writable'
    writable.mkdir(mode=0o744)
    with unittest.mock.patch('site.getsitepackages', return_value=[str(not_writable), str(writable)]):
        result = manager_cls()._site_packages(assert_writable=True)
        assert result == str(writable)


def test_Manager__site_packages__no_writable(manager_cls, tmpdir):
    not_writable = Path(tmpdir) / 'not-writable'
    not_writable.mkdir(mode=0o444)
    if os.access(not_writable, os.W_OK | os.X_OK):
        pytest.skip("Unusual filesystem behaviour (seen in kubernetes gitlab-ci runners)")
    with unittest.mock.patch('site.getsitepackages', return_value=[str(not_writable)]):
        with pytest.raises(RuntimeError, match="No writable site-packages directory"):
            manager_cls()._site_packages(assert_writable=True)
