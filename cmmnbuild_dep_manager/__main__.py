from __future__ import annotations

"""CommonBuild Dependency Manager

Copyright (c) CERN 2015-2020

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Authors:
    T. Levens   <tom.levens@cern.ch>
    P. Elson   <philip.elson@cern.ch>
"""

import argparse
import functools
import inspect
import logging
import typing

from . import Manager

_KWARG_PREFIX = "__kwarg__"


def _print_help(parser: argparse.ArgumentParser, _: typing.Any) -> None:
    parser.print_help()
    parser.exit()


def configure_parser(parser: argparse.ArgumentParser) -> None:
    parser.description = "A CLI to cmmnbuild_dep_manager"
    parser.set_defaults(handler=functools.partial(_print_help, parser))
    subparsers = parser.add_subparsers(dest="subcommand")
    # generate subparsers for the various methods of the Manager class

    method_name = [
        "class_doc",
        "class_hints",
        "class_list",
        "class_path",
        "class_search",
        "install",
        "is_installed",
        "is_registered",
        "is_resolved",
        "jar_path",
        "jars",
        "java_requirements",
        "list",
        "lockfile_generate",
        "lockfile_install",
        "register",
        "registered_modules",
        "resolve",
        "stubgen",
        "uninstall",
        "unregister",
    ]

    for attr_name in method_name:
        obj = getattr(Manager, attr_name)
        parameters = dict(inspect.signature(obj).parameters)
        parameters.pop("self")

        method_subparser = subparsers.add_parser(
            attr_name,
            help=obj.__doc__.strip().splitlines()[0],
            formatter_class=argparse.RawTextHelpFormatter,
        )

        method_parameters_group = method_subparser.add_argument_group("Method parameters")

        for param_name, param in parameters.items():

            if param.kind == param.POSITIONAL_OR_KEYWORD:
                method_parameters_group.add_argument(
                    f'--{param_name}',
                    dest=f"{_KWARG_PREFIX}{param_name}",
                    help=f"The {param_name} keyword argument to {attr_name}",
                    metavar=param_name,
                )
                method_subparser.add_argument(
                    f'{param_name}', nargs='?',
                    # We prefer people to use named arguments instead of
                    # positional ones, so hide them from the help.
                    help=argparse.SUPPRESS,
                )
            elif param.kind == param.VAR_POSITIONAL:
                method_parameters_group.add_argument(
                    f'{param_name}', nargs='*', default=[],
                )
            else:
                raise ValueError(f"Unhandled param type {param.kind}")

        method_subparser.set_defaults(handler=manager_method_handler)


def manager_method_handler(args: argparse.Namespace) -> None:
    """
    Handle the execution of a cmmnbuild_dep_manager.Manager method from CLI
    """
    mgr = Manager()
    mgr.set_logging_level(logging.INFO)

    fn_args = list(getattr(args, 'args', []))
    fn_kwargs = {}

    mgr_method = getattr(mgr, args.subcommand)

    for name, value in vars(args).items():
        if name in ['args', 'handler', 'subcommand']:
            # Names that are used for figuring out the action.
            continue
        if value is None:
            continue

        if name.startswith(_KWARG_PREFIX):
            name = name[len(_KWARG_PREFIX):]

        if name in fn_kwargs:
            raise ValueError(f"Duplicate parameter \"{name}\" provided ")

        fn_kwargs[name] = value

    return_value = mgr_method(*fn_args, **fn_kwargs)

    if return_value is not None:
        if isinstance(return_value, str):
            print(return_value)
        elif hasattr(return_value, '__iter__'):
            for r in return_value:
                print(r)
        else:
            print(return_value)


def main() -> None:
    logging.basicConfig(level=logging.INFO, format='%(levelname)s: %(message)s')

    parser = argparse.ArgumentParser()
    configure_parser(parser)
    args = parser.parse_args()
    args.handler(args)


if __name__ == '__main__':
    main()
