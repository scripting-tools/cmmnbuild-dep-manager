"""
Top-level cmmnbuild-dep-manager package, and the primary entry-point for its functionality.
"""
from . import jvm as jvm
from ._manager import Manager as Manager  # explicit export
from ._version import version as __version__

#: The :class:`~cmmnbuild_dep_manager.jvm._manager.JVMManager` instance which influences the JVM startup of :class:`Manager`.
JVM = Manager.JVM
