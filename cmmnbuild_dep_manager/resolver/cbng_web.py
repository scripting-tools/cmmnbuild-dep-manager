from __future__ import annotations

import concurrent.futures
import logging
import pathlib
import platform
import sys
import typing
import xml.etree.ElementTree as ET

import requests
import requests.exceptions

from .. import resolver

LOG = logging.getLogger(__package__)


class CbngWebResolver(resolver.MultiPhaseResolver):
    dependency_variable = '__cmmnbuild_deps__'
    description = 'CBNG Web Service'
    CBNG_WEB_SERVICE = 'https://acc-py-repo.cern.ch/cbng-web-resolver'
    CBNG_WEB_ENDPOINT = '/resolve/productxml'
    CBNG_WEB_URL = CBNG_WEB_SERVICE + CBNG_WEB_ENDPOINT

    @classmethod
    def is_available(cls) -> bool:
        # check if CBNG web service is reachable
        try:
            with resolver._requests_no_insecure_warning():
                requests.get(cls.CBNG_WEB_URL, timeout=5.0, verify=False)
            return True
        except requests.exceptions.RequestException:
            try:
                hostname = platform.node()
                if 'cern.ch' in hostname:
                    logging.warning((
                        'This appears to be a CERN machine ({0}), but CBNG is not available. Will try ' +
                        'to resolve dependencies from public repositories, which may not work for ' +
                        'CERN internal modules.'
                    ).format(hostname))
            except Exception:
                pass
            return False

    def __init__(self, dependencies: typing.Sequence[typing.Union[str, typing.Dict[str, str]]]) -> None:
        """Resolve dependencies using the CBNG web service"""
        valid_deps = self._get_valid_dependencies(dependencies)

        pxml = self.build_product_xml(valid_deps)

        # Post product.xml to CBNG web service
        LOG.info('resolving dependencies using CBNG web service')

        with resolver._requests_no_insecure_warning():
            response = requests.post(
                self.CBNG_WEB_URL, data=ET.tostring(pxml), verify=False,
            )
        response.raise_for_status()

        #: The dependencies resolved by CBNG based on the set of input dependencies.
        self.resolved_artifacts = self._resolved_artifacts_from_response(
            response.json(),
        )

        LOG.info(
            f"CBNG resolved {len(self.resolved_artifacts)} artifacts: "
            f"{', '.join([dependency.artifact_id for dependency in self.resolved_artifacts])}",
        )

    def _resolved_artifacts_from_response(
            cls,
            resolved_deps_response: typing.List[typing.Dict[str, str]],
    ) -> typing.Tuple[resolver.Artifact, ...]:
        result = []
        expected_keys = {'artifactId', 'groupId', 'version', 'uri'}
        for dependency in resolved_deps_response:
            missing_keys = expected_keys - set(dependency)
            if missing_keys:
                raise ValueError(
                    f"Fields {', '.join(missing_keys)} missing from response",
                )
            result.append(
                resolver.Artifact(
                    artifact_id=dependency['artifactId'],
                    group_id=dependency['groupId'],
                    version=dependency['version'],
                    uri=dependency['uri'],
                ),
            )
        return tuple(result)

    def _get_valid_dependencies(self, dependencies: typing.Sequence[typing.Union[str, dict[str, str]]]) -> typing.List[typing.Dict[str, str]]:
        valid_deps = []
        errors = []
        for dep in dependencies:
            if not isinstance(dep, dict):
                errors.append(f'Invalid dependency declaration: {dep!r}')
            elif  'groupId' not in dep:
                errors.append(f'The dependency declaration must include a groupId, got: {dep!r}')
            else:
                valid_deps.append({str(k): str(v) for k, v in dep.items()})
        if errors:
            raise ValueError("Invalid dependencies provided: \n " + "\n ".join(errors))
        return valid_deps

    @classmethod
    def build_product_xml(cls, valid_deps: typing.Sequence[typing.Dict[str, str]]) -> ET.Element:
        # Generate product.xml
        cmmnbuild_dep_mgr = __package__.split('.')[0]
        pxml = ET.Element('products')
        pxml_prod = ET.SubElement(
            pxml, 'product', attrib={
                'name': cmmnbuild_dep_mgr,
                'version': sys.modules[cmmnbuild_dep_mgr].__version__,
                'directory': cmmnbuild_dep_mgr,
            },
        )
        pxml_deps = ET.SubElement(pxml_prod, 'dependencies')
        for dep in valid_deps:
            ET.SubElement(pxml_deps, 'dep', attrib=dep)
        return pxml

    def save_jars(self, dir: str) -> None:
        """Save the resolved artifacts to the given directory, removing any pre-existing JARs."""
        # Save metadata about resolved packages
        # json.dump(self.resolved_deps, open(os.path.join(dir, "dependencies.json"), "w"))
        self.download_jars(pathlib.Path(dir), self.resolved_artifacts)

    @classmethod
    def download_jars(
        cls, destination: pathlib.Path,
        artifacts: typing.Tuple[resolver.Artifact, ...],
    ) -> None:
        existing_jars = {jarfile.name for jarfile in destination.glob('*.jar')}
        desired_jars = {artifact.filename_from_uri for artifact in artifacts}

        jars_to_remove = existing_jars - desired_jars
        if jars_to_remove:
            LOG.info(f'removing {len(jars_to_remove)} existing jars')

        jars_to_keep = existing_jars & desired_jars
        if jars_to_keep:
            LOG.info(f"{len(jars_to_keep)} of {len(desired_jars)} JARs already downloaded")

        with concurrent.futures.ThreadPoolExecutor(6) as executor:
            # Create a pool of threads for parallel downloading
            download_tasks = {
                executor.submit(artifact.download_jar, destination): artifact.uri
                for artifact in artifacts
                if artifact.filename_from_uri not in existing_jars
            }

            # Meanwhile, remove the existing JARs that we don't want.
            for jar_to_remove in jars_to_remove:
                (destination / jar_to_remove).unlink()

            # Wait for the downloads to complete and check results are exceptions
            # (potentially raising if they are).
            for task in concurrent.futures.as_completed(download_tasks):
                task.result()

    @classmethod
    def get_help(cls, classnames: typing.Iterable[str], class_info: typing.Dict[str, typing.List[typing.Tuple[str, str]]]) -> None:
        for classname in classnames:
            lst = classname.split('.')
            data = {
                'u': 'https://artifactory.cern.ch/development',
                'r': '/'.join(lst),
                'o': lst[0],
                'a': lst[1],
            }
            for package, version in class_info[classname]:
                print(
                    'Info for "{0}" in "{1}" version "{2}"'.format(
                        classname, package, version,
                    ),
                )
                data['v'] = version
                data['p'] = package
                url = (
                    'JavaDoc: {u}/{o}/{a}/{p}/{v}/{p}-{v}-javadoc.jar'
                    '!/index.html?{r}.html'
                )
                print(url.format(**data))
                url = (
                    'Sources: {u}/{o}/{a}/{p}/{v}/{p}-{v}-sources.jar'
                    '!/{r}.java'
                )
                print(url.format(**data))
