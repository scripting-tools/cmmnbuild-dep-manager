from __future__ import annotations

import abc as _abc
import contextlib
import dataclasses
import logging
import pathlib
import typing
import warnings

import requests
import requests.exceptions
from urllib3 import exceptions as _r_exceptions


def resolvers() -> typing.List[typing.Type[Resolver]]:
    """Return all supported resolvers, ordered by precedence"""
    from . import cbng_web, gradle
    return [cbng_web.CbngWebResolver, gradle.GradleResolver]


DependencyType = typing.Union[str, typing.Dict]


class Resolver(_abc.ABC):
    """
    An abstract base class for a dependency resolver.

    Known implementations include
    :class:`cmmnbuild_dep_manager.resolver.cbng_web.CbngWebResolver`
    and
    :class:`cmmnbuild_dep_manager.resolver.gradle.GradleResolver`

    """
    #: The name of the variable that must be available on a module which wishes
    #: to be able to have its dependencies resolved by this resolver (str).
    dependency_variable: str = '__the_name_of_the_module_attribute_for_deps__'

    #: A short description of the resolver, for logging/info purposes
    #: only (str).
    description: str = 'The description of the resolver.'

    @classmethod
    @_abc.abstractmethod
    def is_available(cls) -> bool:
        pass

    @_abc.abstractmethod
    def __init__(self, dependencies: typing.Iterable[DependencyType]) -> None:
        pass

    @_abc.abstractmethod
    def save_jars(self, dir: str) -> None:
        pass

    @classmethod
    @_abc.abstractmethod
    def get_help(cls, classnames: typing.Iterable[str], class_info: typing.Dict[str, typing.List[typing.Tuple[str, str]]]) -> None:
        pass


@contextlib.contextmanager
def _requests_no_insecure_warning() -> typing.Generator[None]:
    with warnings.catch_warnings():
        warnings.filterwarnings(
            "ignore", category=_r_exceptions.InsecureRequestWarning,
        )
        yield


LOG = logging.getLogger(__package__)


@dataclasses.dataclass(frozen=True)
class Artifact:
    artifact_id: str
    group_id: str
    version: str
    uri: str

    @property
    def filename_from_uri(self) -> str:
        """Guess the name of the JAR based on the URI."""
        return self.uri.rsplit('/', 1)[1]

    def download_jar(self, destination: pathlib.Path) -> None:
        """Download the JAR into the given directory.
        """
        # Function to download a single jar into a given directory
        with _requests_no_insecure_warning():
            jar_request = requests.get(self.uri, stream=True, timeout=20.0, verify=False)
        jar_request.raise_for_status()
        LOG.info(
            'downloading {group}:{artifact}:{version}'.format(
                group=self.group_id, artifact=self.artifact_id, version=self.version,
            ),
        )
        with (destination / self.filename_from_uri).open("wb") as jar_file:
            for chunk in jar_request.iter_content(chunk_size=1024):
                jar_file.write(chunk)


class MultiPhaseResolver(Resolver, _abc.ABC):
    """
    An abstract Resolver which has a strong separation between the "resolve"
    and the "fetch/download" phases.

    """
    resolved_artifacts: typing.Tuple[Artifact, ...]

    @classmethod
    @_abc.abstractmethod
    def download_jars(cls, destination: pathlib.Path, artifacts: typing.Tuple[Artifact, ...]) -> None:
        """Download the given :class:`Artifact` instances into the given
           directory, removing any pre-existing JARs in the directory if necessary.

        """
        pass
