from __future__ import annotations

import glob
import logging
import os
import re
import shutil
import subprocess
import sys
import tempfile
import typing
import zipfile

import requests

from .. import resolver


class GradleResolver(resolver.Resolver):
    dependency_variable = '__gradle_deps__'
    description = 'Pure Gradle - when the CERN CBNG is not available'

    @classmethod
    def is_available(cls) -> bool:
        # check if maven central is reachable
        try:
            requests.get('http://repo1.maven.org/maven2/', timeout=5.0)
            return True
        except:
            return False

    def __init__(self, dependencies: typing.Sequence[str | dict[str, str]]) -> None:
        """Resolve dependencies using Gradle and public repos"""
        self.log = logging.getLogger(__package__)

        extra_repos = []
        valid_deps = []
        for dep in dependencies:
            if isinstance(dep, dict):
                try:
                    groupId = dep['groupId']
                    artifactId = dep['product'] if 'product' in dep else dep['artifactId']
                    version = dep.get('version', '+')
                    if 'repository' in dep:
                        extra_repos.append(str(dep['repository']))
                    dtype = dep.get('type', None)
                    dep = f'{groupId}:{artifactId}:{version}'
                    if dtype:
                        dep = f'{dtype}({dep})'
                except Exception as e:
                    self.log.warning(f'IGNORING __gradle_deps__ dependency "{dep}": invalid dict {e}')
                    continue
            dep_type = None
            dep_type_match = re.match('(\\w+)\\([\'"]?(.*?)[\'"]?\\)', dep)
            if dep_type_match:
                dep_type = dep_type_match.group(1)
                dep = dep_type_match.group(2)
            if not re.match('[a-z0-9_.:+\\-]', dep):
                self.log.warning(f'IGNORING __gradle_deps__ dependency "{dep}": contains invalid characters.')
            elif dep.count(':') != 2:
                self.log.warning(
                    f'IGNORING __gradle_deps__ dependency "{dep}": it is not in gradle format '
                    + '-> "<group>:<product>:<version>"',
                )
            else:
                dep_str = f"'{dep}'"
                if dep_type:
                    dep_str = f'{dep_type}({dep_str})'
                self.log.info(f'Resolving {dep_str} using Gradle')
                valid_deps.append(dep_str)

        gradle_buildscript = '''
            plugins {
                id 'java'
            }
            apply plugin: 'maven-publish'

            repositories {
                mavenCentral()
                ''' + '\n'.join(extra_repos) + '''
            }

            configurations {
                pythondeps
            }
            task getJars(type: Copy) {
                from configurations.pythondeps
                into "$buildDir/jars"
            }
        '''
        gradle_buildscript += '\ndependencies {\n'
        for dep in valid_deps:
            gradle_buildscript += f'    pythondeps {dep}\n'
        gradle_buildscript += '\n}\n'

        self.temp_dir = tempfile.mkdtemp(prefix="cmmn-dep-manager-gradle-resolve-")
        self.log.info(f'Running Gradle in "{self.temp_dir}"...')
        with zipfile.ZipFile(os.path.join(os.path.dirname(__file__), 'gradle-wrapper.zip')) as gradle:
            gradle.extractall(self.temp_dir)
        with open(os.path.join(self.temp_dir, 'build.gradle'), 'w') as buildfile:
            buildfile.write(gradle_buildscript)
        gradle_wrapper = 'gradlew.bat' if sys.platform.startswith('win') else 'chmod a+x ./gradlew && ./gradlew'
        retcode = subprocess.call(gradle_wrapper + ' getJars', cwd=self.temp_dir, shell=True)
        if retcode != 0:
            raise OSError('error executing gradle!')
        self.gradle_dir = self.temp_dir

    def __del__(self) -> None:
        if os.path.exists(self.temp_dir):
            self.log.info(f'cleaning up {self.temp_dir}')
            shutil.rmtree(self.temp_dir, ignore_errors=True)

    def _clean_jars_dir(self, jars_dir: str) -> None:
        # Remove existing jars
        old_jars = glob.glob(os.path.join(jars_dir, '*.jar'))
        self.log.info(f'removing {len(old_jars)} jars from {jars_dir}')
        for jar in old_jars:
            os.remove(jar)

    def save_jars(self, dir: str) -> None:
        self._clean_jars_dir(dir)
        self.log.info(f'> putting jars into {dir}')
        for jar in glob.glob(os.path.join(self.gradle_dir, 'build', 'jars', '*.jar')):
            self.log.info(f'retrieving {os.path.basename(jar)}')
            shutil.copy(jar, dir)

    @classmethod
    def get_help(cls, classnames: typing.Iterable[str], class_info: typing.Dict[str, typing.List[typing.Tuple[str, str]]]) -> None:
        print("Online JavaDoc not supported for Gradle dependencies!")
