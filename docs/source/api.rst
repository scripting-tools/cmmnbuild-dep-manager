.. _API_docs:

cmmnbuild-dep-manager API documentation
========================================

.. rubric:: Modules

.. autosummary::
   :toctree: api

   .. Add the sub-packages that you wish to document below

   cmmnbuild_dep_manager
   cmmnbuild_dep_manager.jvm._manager
   cmmnbuild_dep_manager.jvm._startup_arguments
   cmmnbuild_dep_manager.resolver
   cmmnbuild_dep_manager.resolver.cbng_web
   cmmnbuild_dep_manager.resolver.gradle
