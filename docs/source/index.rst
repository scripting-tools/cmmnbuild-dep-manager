cmmnbuild-dep-manager
=====================

Introduction
------------

Manages CERN's Java dependencies across multiple Python packages


Installation
------------

Using the `acc-py Python package index
<https://wikis.cern.ch/display/ACCPY/Getting+started+with+acc-python#Gettingstartedwithacc-python-OurPythonPackageRepositoryrepo>`_
``cmmnbuild-dep-manager`` can be pip installed with::

   pip install cmmnbuild-dep-manager


Documentation contents
----------------------

.. toctree::
    :maxdepth: 1
    :hidden:

    self

.. toctree::
    :caption: Reference docs
    :maxdepth: 1

    api
    genindex
