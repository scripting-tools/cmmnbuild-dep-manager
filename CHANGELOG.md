# Changelog

## [2.4.1] - 20.05.2020
### Changed 
- add support for the JPype import system (`Manager.imports()`)

## [2.2.5] - 21.10.2019
### Changed 
- avoid downloading jars at every execution